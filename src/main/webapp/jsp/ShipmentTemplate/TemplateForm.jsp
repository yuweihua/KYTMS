<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: sundezeng
  Date: 2017/12/04
  Time: 21:27
  运单模板表单
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>

<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>运单模板</title>
    <script src="/Content/scripts/jquery/jquery-1.10.2.min.js"></script>
    <link href="/Content/styles/font-awesome.min.css" rel="stylesheet"/>
    <link href="/Content/scripts/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>
    <script src="/Content/scripts/plugins/jquery-ui/jquery-ui.min.js"></script>

    <link href="/Content/scripts/bootstrap/bootstrap.min.css" rel="stylesheet"/>
    <link href="/Content/scripts/bootstrap/bootstrap.extension.css" rel="stylesheet"/>
    <script src="/Content/scripts/bootstrap/bootstrap.min.js"></script>
    <script src="/Content/scripts/plugins/datepicker/WdatePicker.js"></script>
    <link href="/Content/scripts/plugins/tree/tree.css" rel="stylesheet"/>
    <link href="/Content/scripts/plugins/datetime/pikaday.css" rel="stylesheet"/>
    <link href="/Content/scripts/plugins/wizard/wizard.css" rel="stylesheet"/>
    <link href="/Content/styles/jet-ui.css" rel="stylesheet"/>
    <script src="/Content/scripts/plugins/tree/tree.js"></script>
    <script src="/Content/scripts/plugins/validator/validator.js"></script>
    <script src="/Content/scripts/plugins/datepicker/DatePicker.js"></script>
    <script src="/Content/scripts/utils/jet-ui.js"></script>
    <script src="/Content/scripts/utils/jet-uuid.js"></script>
    <script src="/Content/scripts/utils/jet-form.js"></script>
    <script src="/Content/scripts/plugins/jquery.md5.js"></script>
    <script src="/Content/scripts/plugins/jqgrid/grid.locale-cn.js"></script>
    <script src="/Content/scripts/plugins/jqgrid/jqgrid.js"></script>
    <link href="/Content/scripts/plugins/jqgrid/jqgrid.css" rel="stylesheet"/>
    <link href="/Content/styles/jet-report.css" rel="stylesheet"/>
    <link href="/Content/styles/jet-bill.css" rel="stylesheet"/>
    <script src="/Content/scripts/plugins/layout/jquery.layout.js"></script>
    <script src="/Content/scripts/utils/jet-pinyin.js"></script>

    <style>
        html, body {
            height: 100%;
            width: 100%;
        }
    </style>
</head>
<body>
<script>
    var feeTypeData;//费用变量存储
    var keyValue = request('keyValue');
    $(function () {
        InitialPage();
        InitControl();
        //changeLiense();
    });
    //初始化数据
    function InitControl() {
        getFeeType();//初始化运单费用类型
        $("#shipmentMethod").ComboBox({ //运输方式
            description: "=请选择运输性质=",
            height: "200px",
            data: top.clientdataItem.TransportMenth
        });
        $("#feeType").ComboBox({ //结算方式
            description: "=请选择结算方式=",
            height: "200px",
            data: top.clientdataItem.Clearing
        });
        $("#taxRate").ComboRadio({ //税率
            data: top.clientdataItem.Logistics_Tax_Rate,
            defaultVaue: '0.11'
        });

        $("#carrierType").ComboBox({ //承运商类型
            description: "==请选择==",
            height: "200px",
            data: top.clientdataItem.CarrierType
        });
        $("#operationPattern").ComboBox({ //作业模式
            description: "=请选择作业模式=",
            height: "200px",
            data: top.clientdataItem.OperationPattern,
        });

        if (!!keyValue) {
            $.SetForm({
                url: "/template/selectBean.action",
                param: {tableName:"JC_SHIPMENT_TEMPLATE", id: keyValue },
                success: function (data) {
                    $("#form1").SetWebControls(data);
                    if(data.carrier != null && data.carrier != undefined){
                        $("#carrier").val(data.carrier.id);
                        $("#carrierName").val(data.carrier.name);
                        $("#carrierName").val(data.carrier.name);
                    }
                    $("#vehicle").val(data.vehicle.id);
                    $("#vehicleName").val(data.vehicle.code);
                    $("#line").val(data.line.id);
                    $("#liense").val(data.liense);
                    $("#lineName").val(data.line.name);
                    if(data.vehicleHead != null && data.vehicleHead != undefined){
                        $("#vehicleHead").val(data.vehicleHead.id);
                        $("#vehicleHeadName").val(data.vehicleHead.code);
                    }
                    if(data.driver != null && data.driver != undefined){
                        $("#driver").val(data.driver.id);
                        $("#driverName").val(data.driver.name);
                    }
                    if(data.carriage != null && data.carriage != undefined){
                        $("#carriage").val(data.carriage.id);
                        $("#carriageName").val(data.carriage.name);
                    }
                    if(data.trailer != null && data.trailer != undefined){
                        $("#trailer").val(data.trailer.id);
                        $("#trailerName").val(data.trailer.name);
                    }
                    GetGrid5() //初始化费用明细
                    var $JGGRID = $("#gridTable5");
                    $JGGRID.jqGrid("clearGridData"); //清空数据
                    $JGGRID.setGridHeight(0)
                    if(data.templateLedgers != null){
                        for(i=0;i<data.templateLedgers.length;i++){
                            var ledgerdetail = data.templateLedgers[i].templateLedgerDetails;
                            $JGGRID.setGridHeight($JGGRID.getGridParam("height") + 28 * ledgerdetail.length)
                            $JGGRID.addRowData(getUUID(),ledgerdetail,"first");
                        }
                    }
                }
            });
        } else {
            $("#feeType").ComboBoxSetValue(0);
            $("#shipmentMethod").ComboBoxSetValue(0);
            $("#operationPattern").ComboBoxSetValue(0);
            GetGrid5(); //初始化费用明细
            initFeeType();
        }
        showInOrOut();
    }
    //初始化页面
    function InitialPage() {
        //layout布局
        $('#layout').layout({
            applyDemoStyles: true,
            onresize: function () {
                $(window).resize()
            }
        });

        $('.profile-content').height($(window).height() - 20);
        //resize重设(表格、树形)宽高
        $(window).resize(function (e) {
            window.setTimeout(function () {
                $('.profile-content').height($(window).height() - 20);
            }, 200);
            e.stopPropagation();
        });
    }


    function initFeeType(){
        $.ajax({
            url: "/feetype/getModelFeeType.action",
            data: {id:"2"},
            async:false,
            type: "post",
            dataType: "json",
            success: function (data) {
                var $JGGRID = $JGGRID = $("#gridTable5");
                $JGGRID.setGridHeight($JGGRID.getGridParam("height") + 28 * data.length)
                $JGGRID.addRowData(getUUID(),data,"first");
            },
        });
    }



    /**
     * 保存单据
     * */
    function savaTemplate() {
        $.removeFromMessage($("#form1"));
        if (!$('#form1').Validform()) {
            return false;
        }
        var postData = $("#form1").GetWebControls(keyValue);
        if( $("#carrier").val() != ""){
            postData['carrier']={
                id: $("#carrier").val(),
                name:$("#carrierName").val()
            }
        }
        if( $("#vehicle").val() != ""){
            postData['vehicle']={
                id:$("#vehicle").val(),
                code:$("#vehicleName").val()
            }
        }
        if( $("#line").val() != ""){
            postData['line']={
                id: $("#line").val(),
                name:$("#lineName").val()
            }
        }
        if( $("#vehicleHead").val() != ""){
            postData['vehicleHead']={
                id: $("#vehicleHead").val(),
                code:$("#vehicleHeadName").val()
            }
        }
        if( $("#driver").val() != ""){
            postData['driver']={
                id: $("#driver").val(),
                name:$("#driverName").val()
            }
        }
        if( $("#carriage").val() != ""){
            postData['carriage']={
                id: $("#carriage").val(),
                name:$("#carriageName").val()
            }
        }
        if( $("#trailer").val() != ""){
            postData['trailer']={
                id: $("#trailer").val(),
                code:$("#trailerName").val()
            }
        }
        //处理台账
        var jggirdData2 = $('#gridTable5').jqGrid("getRowData");
        var ledgers = new Array(); //台账
        var LedgerDetails = new Array(); //台账明细
        var countAmount =0;
        var countInput = 0;
        for(i=0;i<jggirdData2.length;i++){
            var feeTypeId;
            var leddetail = jggirdData2[i]
            countAmount=countAmount+parseFloat(leddetail.amount)
            countInput=countInput+parseFloat(leddetail.input)
            for(var j in feeTypeData){
                if(feeTypeData[j] ==leddetail['feeType.name']){
                    feeTypeId=j
                }
            }

            var LedgerDetail = {
                id:leddetail.id,
                feeType : {
                    id:feeTypeId
                },
                amount:leddetail.amount,
                taxRate:leddetail.taxRate,
                input:leddetail.input
            }
            LedgerDetails.push(LedgerDetail)
        }


        var ledger = {
            amount:countAmount,
            taxRate:0,
            input:countInput,
            type:1,
            cost:1
        }
        ledger['templateLedgerDetails'] = LedgerDetails;
        ledgers.push(ledger);
        postData['templateLedgers'] = ledgers;
        var params = {str:JSON.stringify(postData)};
        var url ="template/saveTemplate.action";

        $.SaveForm({
            url: url,
            param:params ,
            loading: "正在保存数据...",
            success: function (data) {
                if (data.type == "validator") {
                    $('#form1').ValidformResule(data.obj);//后台验证数据
                } else if (data.type) {
                    $.currentIframe().$("#gridTable").trigger("reloadGrid");
                    dialogClose();//关闭窗口
                } else {
                    dialogAlert(data.obj, -1);
                }
            }
        })

    }



    function selectCarrier(){
        dialogOpen({
            id: "CarrierFrom",
            title: '承运商选择',
            url: 'jsp/Carrier/CarrierIndex.jsp?status=1',
            width: "1050px",
            height: "450px",
            callBack: function (iframeId) {
                var resule = top.frames[iframeId].getCarrier();
                top.frames[iframeId].dialogClose();//关闭窗口
                $("#carrier").val(resule.id)
                $("#carrierName").val(resule.name)
                for(var i in top.clientdataItem.CarrierType){
                    if(top.clientdataItem.CarrierType[i] ==resule.carrierType){
                        $("#carrierType").ComboBoxSetValue(i);
                    }
                }
                //changeLiense();
            }
        });
    }
    /**
     * 线路选择
     */
    function selectLine(){
        dialogOpen({
            id: "LineFrom",
            title: '线路选择',
            url: 'jsp/Line/LineIndex.jsp?status=1',
            width: "1050px",
            height: "450px",
            callBack: function (iframeId) {
                var resule = top.frames[iframeId].getLine();
                top.frames[iframeId].dialogClose();//关闭窗口
                $("#line").val(resule.id)
                $("#lineName").val(resule.name)
            }
        });
    }
    /**
     * 车型选择
     * @param self
     */
    function selectVehicle (self){
        dialogOpen({
            id: "VehicleFrom",
            title: '车型选择',
            url: 'jsp/vehicle/VehicleIndex.jsp?status=1',
            width: "1050px",
            height: "450px",
            callBack: function (iframeId) {
                var resule = top.frames[iframeId].getVehicle();
                top.frames[iframeId].dialogClose();//关闭窗口
                $("#vehicle").val(resule.id)
                $("#vehicleName").val(resule.code)
            }
        });
    }

    /**
     * 车头选择
     * @param self
     */
    function getVehicleHead (self){
        dialogOpen({
            id: "VehicleFrom",
            title: '车头选择',
            url: 'jsp/VehicleHead/VehicleHeadIndex.jsp?status=1&shipmentSign=1',
            width: "1050px",
            height: "450px",
            callBack: function (iframeId) {
                var resule = top.frames[iframeId].getVehicleHead();
                $.SetForm({
                    url: "/vehicleHead/getVehicleHeadInfo.action",
                    param: {id:resule },
                    success: function (data) {
                        if(data.carriage != null && data.carriage != undefined){
                            $("#carriage").val(data.carriage.id);
                            $("#carriageName").val(data.carriage.name);
                        }
                        if(data.driver != null && data.driver != undefined){
                            $("#driver").val(data.driver.id);
                            $("#driverName").val(data.driver.name);
                        }
                        if(data.trailer != null && data.trailer != undefined){
                            $("#trailer").val(data.trailer.id);
                            $("#trailerName").val(data.trailer.name);
                        }
                        if(data.vehicleHead != null && data.vehicleHead != undefined){
                            $("#vehicleHead").val(data.vehicleHead.id);
                            $("#vehicleHeadName").val(data.vehicleHead.code);
                        }
                    }
                });
                top.frames[iframeId].dialogClose();//关闭窗口
            }
        });
    }
    /**
     * 车厢选择
     * @param self
     */
    function getCarriage (self){
        dialogOpen({
            id: "CarriageFrom",
            title: '车厢选择',
            url: 'jsp/Carriage/CarriageIndex.jsp?status=1&shipmentSign=1',
            width: "1050px",
            height: "450px",
            callBack: function (iframeId) {
                var resule = top.frames[iframeId].getCarriage();
                $("#carriage").val(resule.id);
                $("#carriageName").val(resule.name);
                top.frames[iframeId].dialogClose();//关闭窗口
            }
        });
    }
    /**
     * 车挂选择
     * @param self
     */
    function getTrailer(){
        dialogOpen({
            id: "TrailerFrom",
            title: '车挂选择',
            url: 'jsp/JcTrailer/TrailerIndex.jsp?status=1&shipmentSign=1',
            width: "1050px",
            height: "450px",
            callBack: function (iframeId) {
                var resule = top.frames[iframeId].getTrailer();
                $("#trailer").val(resule.id);
                $("#trailerName").val(resule.name);
                top.frames[iframeId].dialogClose();//关闭窗口
            }
        });
    }
    /**
     * 司机选择
     * @param self
     */
    function getDriver(){
        dialogOpen({
            id: "DriverFrom",
            title: '司机选择',
            url: 'jsp/BaseData/DriverSet/DriverIndex.jsp?status=1',
            width: "1050px",
            height: "450px",
            callBack: function (iframeId) {
                var resule = top.frames[iframeId].getDriver();
                $("#driver").val(resule.id);
                $("#driverName").val(resule.name);
                top.frames[iframeId].dialogClose();//关闭窗口
            }
        });
    }

    //根据作业模式显示内部或外部车辆信息
    function showInOrOut(){
        var value = $("#operationPattern").attr('data-value');
        if(value == 0){
            $("#carrierName").removeAttr('readonly');
            $("#carrierName").attr('disabled',"disabled");
            $("#carrierNameSpan").removeAttr("onclick");
            if(isCreate) {
                $("#gridTable5").jqGrid("clearGridData"); //清空数据
            }
        }else{
            $("#carrierName").removeAttr('disabled');
            $("#carrierName").attr('readonly',"readonly");
            $("#carrierNameSpan").attr("onclick","selectCarrier()");
            if(isCreate){
                initFeeType();
            }
        }
    }




    function GetGrid5() {
        function isInteger(obj) {
            reg =/^[-\+]?\d+(\.\d+)?$/;
            if (!reg.test(obj)) {
                return false;
            } else {
                return true;
            }
        }
        var selectedRowIndex = 0;
        var $gridTable = $('#gridTable5');
        var numberValidator = function(val, nm){
            var boolean = isInteger(val)
            if(boolean){
                return [true,parseFloat(val)];
            }
            return  [false,"请输入正确数字"];
        }
        $gridTable.jqGrid({
//                url: "",
            datatype: "json",
            height: 0,
            autowidth: true,
            cellEdit:true,
            cellsubmit:"clientArray",
            colModel: [
                { label: "主键", name: "id", index: "id", hidden: true },
                { label: "主键", name: "feeType.id", index: "id", hidden: true },
                { label: "费用名称", name: "feeType.name",  width: 200,resizable:false,editable:true,edittype:'select', align: "center",editoptions: {value: feeTypeData}},
                { label: "税率", name: "taxRate",  width: 100,resizable:false, align: "center",editable:true,edittype:'select', editoptions: {value: top.clientdataItem.Logistics_Tax_Rate}},
                { label: "金额", name: "amount",  index: "target", resizable:false, width: 130, align: "center",edittype: "text" ,editable:true,editrules: { required: true, custom:true, custom_func: numberValidator},},
                { label: "税金", name: "input", index: "url",resizable:false,  width: 130, align: "center"},
                { label: "备注信息", name: "description", resizable:false, index: "description", width: 200,edittype: "text", align: "center" ,editable:true },
                { label: '<button type="button" onclick="addJgGirdNullData2()" class="btn btn-success btn-xs">添加费用</button>',   sortable: false, width: 40, align: "center",
                    formatter: function (cellvalue, options, rowObject) {
                        return '<button type="button" onclick="deleteJgGirdNullData2($(this))" class="btn btn-danger btn-xs">删除费用</button>'
                    }}
            ],
            pager: false,
            rownumbers: true,
            shrinkToFit: false,
            gridview: true,
            footerrow: true,
            caption: "费用信息",
            afterSaveCell:function(rowid, cellname, value, iRow, iCol){
                //计算税率
                var rowData = $(this).getRowData(rowid)
                var taxRate = parseFloat(rowData.amount / (1 + parseFloat(rowData.taxRate)) * parseFloat(rowData.taxRate));
                var taxRate =taxRate.toFixed(${SystemConfig.inputRoundNumber}) ;
                rowData.input = taxRate;
                $(this).setRowData(rowid,rowData)
                feeTypeTotal($(this))
                //计算合计


            },
            gridComplete: function () {
                var number = $(this).getCol("amount", false, "sum");
                var weight = $(this).getCol("input", false, "sum");
                //合计
                $(this).footerData("set", {
                    "feeType.name": "合计：",
                    "amount": "金额:" + number,
                    "input": "税金 :" + weight,
                });
            },
        });
    }
    /**
     * 费用合计
     */
    function feeTypeTotal(JGGIRD){
        var amount = JGGIRD.getCol("amount", false, "sum");
        var input = JGGIRD.getCol("input", false, "sum");
        //合计
        JGGIRD.footerData("set", {
            "feeType.name": "合计：",
            "amount": "金额:" + amount,
            "input": "税率:" + input,
        });
    }
    function deleteJgGirdNullData2 (data){
        var $JGGRID = $("#gridTable5");
        data.parent().parent().remove();
        $JGGRID .setGridHeight($JGGRID.getGridParam("height") - 28)
        feeTypeTotal($JGGRID)
    }
    function addJgGirdNullData2 (){
        var model={
            id:null,
            name:null,
            number:0,
            weight:0,
            volume:0,
            value:0,
            description:null,

        }
        var $JGGRID = $("#gridTable5")
        $JGGRID.setGridHeight($JGGRID.getGridParam("height") + 28)
//            $JGGRID.jqGrid("addRowData",null,"first");
        $JGGRID.addRowData(getUUID(),model,"first");
    }
    function getFeeType(){
        $.ajax({
            url: "/feetype/getFeeType.action",
            data: {id:"2.1"},
            async:false,
            type: "post",
            dataType: "json",
            success: function (data) {
                feeTypeData =data;
            },
        });

    }

    //根据承运商类型设置车牌号是否可编辑
    function changeLiense(){
        var value = $("#carrierType").attr('data-value');
        if(value == 0){
            $("#outDriver").attr('disabled',true);
            $("#liense").attr('disabled',true);
        }else{
            $("#outDriver").removeAttr('disabled');
            $("#liense").removeAttr('disabled');
        }
    }
</script>
<div class="ui-layout" id="layout" style="height: 100%; width: 100%;">
    <div class="ui-layout-center">
        <div class="center-Panel">
            <div class="profile-content" style="background: #fff;">
                <div id="ShipmentInfo" class="flag">
                    <form id="form1">
                        <table class="form" style="margin-top: 20px;">
                            <input id="id" name="id" type="hidden" value=""/>
                            <input id="status" name="status" type="hidden" value=""/>
                            <input id="carrier" name="carrier.id" type="hidden" value=""/>
                            <input id="vehicle" name="vehicle.id" type="hidden" value=""/>
                            <input id="line" name="line.id" type="hidden" value=""/>
                            <tr>
                                <td class="formTitle">模板名称<font face="宋体">*</font></td>
                                <td class="formValue">
                                    <input id="name"   type="text" class="form-control"  onKeyUp="pinyinQuery('name','code')" isvalid="yes" checkexpession="NotNull"  />
                                </td>
                                <td class="formTitle">模板代码<font face="宋体">*</font></td>
                                <td class="formValue">
                                    <input id="code" disabled = "disabled"  type="text" class="form-control"  />
                                </td>

                                <th class="formTitle">运输协议号</th>
                                <td class="formValue">
                                    <input id="tan"  type="text" class="form-control" maxlength="50"/>
                                </td>
                            </tr>
                            <tr>
                                <th class="formTitle">作业模式<font face="宋体">*</font></th>
                                <td class="formValue">
                                    <div id="operationPattern" type="select" class="ui-select" isvalid="yes" checkexpession="NotNull"
                                         onchange="showInOrOut()">
                                        <ul>
                                        </ul>
                                    </div>
                                </td>
                                <th class="formTitle">承运商</th>
                                <td class="formValue">
                                    <input id="carrierName"  onclick="selectCarrier()"   type="text"  class="form-control" />
                                    <span id="carrierNameSpan" class="input-button" onclick="selectCarrier()" title="请选择承运商" >.......</span>
                                </td>
                                <th class="formTitle">承运商类型</th>
                                <td class="formValue">
                                    <div id="carrierType" disabled="disabled" name="carrierType" type="select" class="ui-select"
                                    ></div>
                                </td>
                            </tr>
                            <tr>
                                <th class="formTitle">付款方式</th>
                                <td class="formValue">
                                    <div id="feeType" type="select" class="ui-select" isvalid="yes" checkexpession="NotNull"><ul> </ul> </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="formTitle">指定车型<font face="宋体">*</font></th>
                                <td class="formValue">
                                    <input id="vehicleName" onclick="selectVehicle($(this));" placeholder="指定派车单车型"  isvalid="yes" readonly  checkexpession="NotNull" type="text" class="form-control"/>
                                    <span class="input-button" ForderBy = "1" onclick="selectVehicle($(this))" title="请选择车型" >.......</span>
                                </td>
                                <th class="formTitle">运输方式</th>
                                <td class="formValue">
                                    <div id="shipmentMethod" type="select" class="ui-select" isvalid="yes" checkexpession="NotNull">
                                        <ul>
                                        </ul>
                                    </div>
                                </td>
                                <th class="formTitle">线路选择<font face="宋体">*</font></th>
                                <td class="formValue">
                                    <input id="lineName"  onclick="selectLine()"  isvalid="yes"  checkexpession="NotNull" type="text" readonly class="form-control" placeholder="线路必须选择"/>
                                    <span class="input-button" onclick="selectLine()" title="请选择线路" >.......</span>
                                </td>
                            <tr >
                                <th class="formTitle">车头</th>
                                <td class="formValue">
                                    <input id="vehicleHead" name="vehicleHead.id" type="hidden" value=""/>
                                    <input id="vehicleHeadName"  type="text"  class="form-control"/>
                                    <span  id="onclick1" class="input-button" onclick="getVehicleHead()" title="车头" >.......</span>
                                </td>
                                <th class="formTitle">司机</th>
                                <td class="formValue">
                                    <input id="driver" name="driver.id" type="hidden" value=""/>
                                    <input id="driverName"  type="text"  class="form-control" />
                                    <span  id="onclick2" class="input-button" onclick="getDriver()" title="司机" >.......</span>
                                </td>
                            </tr>--%>
                            <tr id="descriptionss">
                                <th class="formTitle">备注</th>
                                <td class="formValue">
                                    <textarea id="description" type="text" style="width: 700px;" class="form-control"></textarea>
                                </td>
                            </tr>
                            <td>
                                <hr style="color: #0a2b1d" width="900px">
                            </td>
                            <tr >

                                <td class="formValue" colspan="6">
                                    <div class="gridPanel">
                                        <table id="gridTable5"></table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div id="bottomField" class="shipmentmanage">
                            <a id="savaTemplate" class="btn btn-success" onclick="savaTemplate()">保存模板</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<style>
    .file {
        position: relative;
        display: inline-block;
        overflow: hidden;
        text-decoration: none;
        text-indent: 0;
        cursor: pointer !important;
    }

    .file input {
        position: absolute;
        font-size: 150px;
        right: 0;
        top: 0;
        opacity: 0;
        cursor: pointer !important;
    }

    .file:hover {
        text-decoration: none;
        cursor: pointer !important;
    }

    .profile-content .formTitle {
        width: 100px;
        height: 20px;
        text-align: right;
    }
    .profile-content .formValue {
        width: 200px;
    }
    .profile-content .input-profile {
        border-radius: 4px;
        width: 60px;
    }
</style>
<%--高德导航--%>
<link href="/Content/styles/jet-gd.css" rel="stylesheet"/>
<%--<script src="/Content/scripts/utils/jet-gdutil.js?v=1.3&key=49df3dbb93cc593e8cceedfe8f8be185"></script>--%>
<script src="https://webapi.amap.com/maps?v=1.3&key=49df3dbb93cc593e8cceedfe8f8be185"></script>
<script src="/Content/scripts/utils/jet-gd.js"></script>
</body>
</html>