package com.kytms.ledgerdetails.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 费用明细
 *
 * @author 臧英明
 * @create 2018-01-18
 */
public interface LedgerDetailDao<LedgerDetail> extends BaseDao<LedgerDetail> {
}

